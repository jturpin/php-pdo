FROM wordpress:php7.4-fpm
RUN apt-get update \
	&& apt-get install -y sendmail \
	&& docker-php-ext-install pdo pdo_mysql

COPY sendmailconf.sh /usr/local/bin/
COPY docker-entrypoint.sh /usr/local/bin/

RUN chmod +x /usr/local/bin/sendmailconf.sh \
	&& rm -rf /var/lib/apt/lists/*

ENTRYPOINT ["docker-entrypoint.sh"]
CMD ["php-fpm"]
