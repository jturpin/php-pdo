#!/bin/bash
host=$(hostname)
echo "127.0.0.1 localhost localhost.localdomain $host" > /tmp/hosts
tail -n +2 /etc/hosts >> /tmp/hosts
cat /tmp/hosts > /etc/hosts
